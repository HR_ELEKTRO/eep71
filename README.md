# EEP71 - Electrical Engineering Project semester 7 #

Dit repository is bedoeld voor studenten en docenten van de opleiding Elektrotechniek van de Hogeschool Rotterdam en wordt gebruikt om studiemateriaal voor de cursus "EEP71 - Electrical Engineering Project semester 7" te verspreiden. 

Alle informatie is te vinden op de [Wiki](https://bitbucket.org/HR_ELEKTRO/eep71/wiki/).

